
var allLinks = document.links;


//Bind the event handler to each link individually
for (var i = 0, n = allLinks.length; i < n; i++) {
    allLinks[i].onclick = function () {
      console.log("The user just clicked: " + this.text);
    };
}



//This keeps track of the input forms.
var inputs, index;

inputs = document.getElementsByTagName('input');
// console.log("There are: "+inputs.length+" input forms.");
for (index = 0; index < inputs.length; ++index) {
    // console.log(inputs[index]);
    inputs[index].onblur = function () {
      console.log("Type into the search bar where it reads" + "' "+this.placeholder+"' ");
      console.log("The user typed in: " + this.value);
      // console.log("The input type was: " + this.type);
    };
}



//This keeps track of the buttons that were clicked
var buttons = document.getElementsByTagName('button');
// console.log("There are: "+buttons.length+" buttons.");
for (var i = 0; i < buttons.length; i++) {
    var button = buttons[i];
    buttons[i].onclick = function () {
      console.log("The button that says " + this.value +" was just clicked.");

    };

}
